<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Workshop Kubernetes - Code2Be</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 3rem;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <a href="https://www.code2.be" target="_blank">
                    <img src="/img/code2be.png" width="30%" />
                </a>
                <div class="title m-b-md">
                    Code2Be
                </div>
                <div class="m-b-md">
                    L'association de développeurs vendéens, retrouvez-nous sur Twitter @code2_be !
                </div>


                <div class="links">
                    <a href="{{route('home')}}">Accueil</a>
                    <a href="{{route('code2be')}}">Code2Be</a>
                    <a href="{{route('aukfood')}}">Aukfood</a>
                    <a href="{{route('ed')}}">Empreinte digitale</a>
                </div>
            </div>
        </div>
    </body>
</html>
