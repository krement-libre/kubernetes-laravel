<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/code2be', function () {
    return view('code2be');
})->name('code2be');

Route::get('/aukfood', function () {
    return view('aukfood');
})->name('aukfood');

Route::get('/empreinte-digitale', function () {
    return view('empreinte-digitale');
})->name('ed');
