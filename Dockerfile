FROM chialab/php:7.2

WORKDIR /app

COPY . /app

RUN composer install --optimize-autoloader --no-dev \
    && cp .env.example .env \
    && php artisan key:generate

CMD php artisan serve --port=80 --host=0.0.0.0
